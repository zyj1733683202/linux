#include <iostream>
#include <cstdio>
#include <string>
#include <ctime>
#include <mutex>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>

// 抢票逻辑，10000票，5线程同时再抢
//tickets是不是就是所谓的临界资源！ tickets-- 是原子的吗？(是安全的吗？)
//为了让多个线程进行切换，线程什么时候可能切换(1. 时间片到了 2. 检测的时间点：从内核态返回用户态的时候)
//对临界区进行加锁
class Ticket{
    private:
    int tickets;
    //int lock; //1 lock--(申请锁) lock++(释放锁) if(lock > 0){lock--}

    //pthread_mutex_t vs std::mutex
    //pthread_mutex_t mtx; //原生线程库,系统级别
    // std::mutex mymtx; //C++ 语言级别

    public:
    Ticket():tickets(1000)
    {
        //pthread_mutex_init(&mtx, nullptr);
    }
    bool GetTicket()
    {
        static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
        //bool变量是否是被所有线程共享的呢？？不是的！
        bool res = true;
        //我要访问临界资源的时候tickets, 需要先访问mtx，前提是所有线程必须得先看到它！
        //那么锁本身，是不是也是临界资源！！！！
        //你如何保证锁本身是安全的！！！
        //原理：lock，unlock-> 是原子的！！（为甚么？）
        //一行代码是原子的：只有一行汇编的情况
        pthread_mutex_lock(&mtx); 
        pthread_mutex_lock(&mtx); 
        // mymtx.lock();
        //执行这部分代码的执行流就是互斥的，串行执行的！
        if(tickets > 0){
            usleep(1000); //1s == 1000ms 1ms = 1000us
            std::cout << "我是[" << pthread_self() << "] 我要抢的票是: " << tickets << std::endl;
            tickets--; //这里看起来就是一行C、C++代码
            printf("");
            //抢票
        }
        else{
            printf("票已经被抢空了\n");
            res = false;
        }
        pthread_mutex_unlock(&mtx);
        // mymtx.unlock();
        return res;
    }
    ~Ticket()
    {
        //pthread_mutex_destroy(&mtx);
    }
};


void *ThreadRoutine(void *args)
{
    Ticket *t = (Ticket*)args;

    //购票的时候，不能出现负数的情况
    // srand((long)time(nullptr));
    while(true)
    {
        if(!t->GetTicket())
        {
            break;
        }
    }
}

int main()
{
    Ticket *t = new Ticket();

    pthread_t tid[5];
    for(int i = 0; i < 5; i++){
        int *id = new int(i);
        pthread_create(tid+i, nullptr, ThreadRoutine, (void*)t);
    }

    for(int i = 0 ; i < 5; i++){
        pthread_join(tid[i], nullptr);
    }
    return 0;
}