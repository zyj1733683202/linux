#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>


int main()
{
    if(fork() == 0){ //child
       printf("command begin...\n");
        execl("/usr/bin/python3", "python", "test.py", NULL);
        //execl("/usr/bin/ls"/*你要执行谁*/, "ls", "-a", "-l", "-i", /*你想怎么执行,命令行上如何执行*/NULL);
        //char *argv[] = {
        //    "ls",
        //    "-a",
        //    "-l",
        //    "-i",
        //    "-n",
        //    NULL
        //};
        //execv("/usr/bin/ls", argv);
        //execlp("ls", "ls", "-a", "-l", "-d", NULL);
        
        //char *argv[] = {
        //    "ls",
        //    "-a",
        //    "-l",
        //    "-i",
        //    "-n",
        //    NULL
        //};
        //execvp("ls", argv);
        //execl("./myexe", "myexe", NULL);//执行当前目录下的myexe
      //  char *env[] = {
      //      "MYENV=hahahahahahahahehehe",
      //      "MYENV1=hahahahahahahahehehe",
      //      "MYENV2=hahahahahahahahehehe",
      //      "MYENV3=hahahahahahahahehehe",
      //      NULL
      //  };
      //  char *argv[] = {
      //      "myexe",
      //      NULL
      //  };
      //  execve("./myexe", argv, env);
        //execle("./myexe", "myexe", NULL, env);
        printf("command end...\n");
        exit(1);
    }

    waitpid(-1, NULL, 0);

    printf("wait child success!\n");
    return 0;
}

