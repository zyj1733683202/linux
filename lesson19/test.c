#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int count = 0;

void HandlerAlarm(int signo)
{
    printf("hello: %d\n", count);

    exit(1);
}


//统计一下，1s，我们的server能够对int递增到多少
int main()
{
    signal(SIGALRM, HandlerAlarm);
    alarm(1); //没有设置alarm信号的捕捉动作(没有自定义)，执行默认动作，终止进程

    while(1){
        count++;
        //为何在这里比较慢？？因为有IO
        //printf("hello : %d\n", count++);
    }
}



//static void Usage(const char *proc)
//{
//    printf("Usage:\n\t %s signo who\n", proc);
//}
//
//void handler(int signo)
//{
//    switch(signo){
//        case 2:
//            printf("hello 比特,get a signal: %d\n", signo);
//            break;
//        case 3:
//            printf("hello world,get a signal: %d\n", signo);
//            break;
//        case 9: //？
//            printf("hello ....,get a signal: %d\n", signo);
//            break;
//        default:
//            printf("hello signo : %d\n", signo);
//            break;
//    }
//    exit(1);
//}
//// ./mytest signo who
////int main(int argc, char *argv[])
//int main()
//{
//    int sig = 1;
//    for(; sig <= 31; sig++){
//        signal(sig, handler);
//    }
//
//    int ret = alarm(30);
//
//    while(1){
//        printf("I am a process: ret: %d\n\n", ret);
//        sleep(5);
//
//        int res = alarm(0); //取消闹钟
//        printf("res: %d\n", res);
//        //abort();
//        //raise(8);
//    }
//
//
//    //if(argc != 3){
//    //    Usage(argv[0]);
//    //    return 1;
//    //}
//
//    //int signo = atoi(argv[1]);
//    //int who = atoi(argv[2]);
//
//    //kill(who, signo);
//
//    //printf("signo: %d, who: %d\n", signo, who);
//}
//

//int main()
//{
//    if(fork() == 0){
//        while(1){
//            printf("I am child ....\n");
//            int a = 10;
//            a /= 0;
//        }
//    }
//
//    int status = 0;
//    waitpid(-1, &status, 0);
//
//    printf("exit code: %d, exit sig: %d, core dump flag: %d\n", (status>>8)&0xFF, status&0x7F, (status>>7) & 1);
//}
//


//void handler(int signo)
//{
//    switch(signo){
//        case 2:
//            printf("hello 比特,get a signal: %d\n", signo);
//            break;
//        case 3:
//            printf("hello world,get a signal: %d\n", signo);
//            break;
//        case 9: //？
//            printf("hello ....,get a signal: %d\n", signo);
//            break;
//        default:
//            printf("hello signo : %d\n", signo);
//            break;
//    }
//    exit(1);
//}
//
//int main()
//{
//    //将所有的信号都进行捕捉
//    //int sig = 1;
//    //for(; sig <= 31; sig++){
//    //    signal(sig, handler);
//    //}
//
//    while(1)
//    {
//    //    int *p = NULL;
//    //    p = (int*)100;  //1
//    //    *p = 100; //2
//        int a = 10;
//        //a /= 0;
//        printf("hello bit\n");
//        sleep(1);
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
