#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

void *thread_run(void* args)
{
    while(1)
    {
        printf("main thread id: 0x%x\n", pthread_self());
        sleep(2);
    }
}


int main()
{

    pthread_t tid;
    pthread_create(&tid, NULL, thread_run, "new thread");


    while(1){
        printf("main thread id: 0x%x\n", pthread_self());
        sleep(1);
    }
}























//pthread_t g_id;
//
//void *thread_run(void *args)
//{
//    pthread_detach(pthread_self());
//    int num = *(int*)args;
//    while(1){
//        printf("我是新线程[%d], 我创建的线程ID是: %lu\n", num,pthread_self());
//        sleep(2);
//        break;
//        //pthread_cancel(g_id);
//        //break;
//        //野指针问题
//        //if(num == 3){
//        //    printf("thread number : %d quit\n", num);
//        //    int *p = NULL;
//        //    *p = 1000;
//        //}
//    }
//
//    //exit(123);
//    //pthread_exit((void*)123);
//    //obj *x = new obj; 
//
//    //return x;
//    return (void*)111;
//}
//
//#define NUM 1
//
//
//int main()
//{
//    g_id = pthread_self();
//
//    pthread_t tid[NUM];
//    for(int i = 0; i < NUM; i++){
//        pthread_create(tid+i, NULL, thread_run, (void*)&i);
//        sleep(1);
//    }
//
//    printf("wait sub thread....\n");
//    sleep(1);
//
//    printf("cancel sub thread ...\n");
//   // pthread_cancel(tid[0]);
//
//    //void *,32,4.   64,8, 指针变量，本身就可以充当某种容器保存数据
//    void *status = NULL;
//
//    //退出信息，异常呢？不需要处理
//    //不要认为，这里的返回值只是int，也可以是其他变量,对象的地址(不能是临时的)
//    int ret = 0;
//    for(int i = 0; i < NUM; i++){
//        ret = pthread_join(tid[i], &status);
//    }
//
//    printf("ret: %d, status: %d\n", ret, (int)status);
//
//    sleep(3);
//   // while(1){
//   //     printf("我是主线程, 我的thread ID: %lu\n", pthread_self());
//
//   //     printf("#########################begin########################\n");
//   //     for(int i = 0; i < NUM; ++i){
//   //         printf("我创建的线程[%d]是: %lu\n", i, tid[i]);
//   //     }
//   //     printf("#########################end########################\n");
//   //     sleep(1);
//   // }
//
//
//}
