#pragma once

#include <iostream>
#include <vector>
#include <semaphore.h>

namespace ns_ring_queue
{
    const int g_cap_default = 10;

    template <class T>
    class RingQueue
    {
    private:
        std::vector<T> ring_queue_;
        int cap_;
        //生产者关心空位置资源
        sem_t blank_sem_;
        // 消费者关心空位置资源
        sem_t data_sem_;

        //不是临界资源
        int c_step_;
        int p_step_;
    public:
        RingQueue(int cap = g_cap_default): ring_queue_(cap), cap_(cap)
        {
            sem_init(&blank_sem_, 0, cap);
            sem_init(&data_sem_, 0, 0);
            c_step_ = p_step_ = 0;
        }
        ~RingQueue() 
        {
            sem_destroy(&blank_sem_);
            sem_destroy(&data_sem_);
        }
    public:
        // 目前高优先级的先实现单生产和单消费
        void Push(const T &in)
        {   
            //生产接口
            sem_wait(&blank_sem_); //P(空位置)
            //可以生产了，可是往哪个位置生产呢？？
            ring_queue_[p_step_]  = in;
            sem_post(&data_sem_);  //V(数据)，释放了信号量资源，表示有资源了

            p_step_++;
            p_step_ %= cap_;//不能出现溢出状况，返回到cap_的状态
        }
        void Pop(T* out)
        {
            //消费接口
            sem_wait(&data_sem_);  //P
            *out = ring_queue_[c_step_];
            sem_post(&blank_sem_);//释放信号量资源，有空了

            c_step_++;
            c_step_ %= cap_;
        }
    };
}