#include<stdio.h>
#include<string.h>
#include<unistd.h>

int main()
{
    printf("hello printf\n");
    fputs("hello fputs\n", stdout);
    //system
    write(1, "hello write\n", 12);
    fork();
    return 0;
//  const char *msg1="标准输出\n";
//  write(1,msg1,strlen(msg1));
//
//  const char *msg2="标准错误\n";
//  write(2,msg2,strlen(msg2));
//  
//
//  printf("hello printf\n");
//  fprintf(stdout,"hello printf\n");
//
//  close(1);
//  return 0; 
}

