#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>


void handler(int signo)
{
    while(1){
        printf("get a signo: %d\n", signo);
        sleep(1);
    }
}


int main()
{
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = handler;
    sigemptyset(&act.sa_mask);

    sigaddset(&act.sa_mask, 3);

    //act.sa_handler = SIG_IGN
    //act.sa_handler = SIG_DFL;
    //本质是修改当前进程的的handler函数指针数组特定内容
    sigaction(2, &act, NULL);

    while(1){
        printf("hello bit!\n");
        sleep(1);
    }

    return 0;
}

//void show_pending(sigset_t *set)
//{
//    printf("curr process pending: ");
//    for(int i = 1; i <= 31; i++){
//        if(sigismember(set, i)){
//            printf("1");
//        }
//        else{
//            printf("0");
//        }
//    }
//
//    printf("\n");
//}
//
//void handler(int signo)
//{
//    printf("%d 号信号被递达了，已经处理完成!\n", signo);
//}
//
//int main()
//{
//    //虽然sigset_t 是一个位图结构，但是不同的OS实现是不一样的，不能让用户直接修改该变量
//    //需要使用特定的函数
//
//    //set是一个变量，该变量在什么地方保存？？和我们之前用到的int，double，没有任何差别，都是在用户栈上
//    //sigset_t set;
//
//    //set |= 1;
//
//    signal(2, handler);
//
//    sigset_t iset, oset;
//
//    sigemptyset(&iset);
//    sigemptyset(&oset);
//
//    sigaddset(&iset, 2);
//    //sigaddset(&iset, 9);
//
//    //1. 设置当前进程的屏蔽字
//    //2. 获取当前进程老的屏蔽字
//    sigprocmask(SIG_SETMASK, &iset, &oset);
//
//    int count = 0;
//    sigset_t pending;
//    while(1){
//        sigemptyset(&pending);
//
//        sigpending(&pending);
//
//        show_pending(&pending);
//
//        sleep(1);
//
//        count++;
//
//        if(count == 10){
//            sigprocmask(SIG_SETMASK, &oset, NULL);
//            //2号信号的默认动作是终止进程，所以看不到现象
//            printf("恢复2号信号,可以被递达了\n");
//        }
//    }
//
//
//
//
//    return 0;
//}
//
//
//
//
//
//
