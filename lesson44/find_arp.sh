#!/bin/bash


sb_net='172.17.0.';
cnt=1

while [ $cnt -le 255 ]
do
    echo "ping $sb_net$cnt"
    ping -c1 $sb_net$cnt &

    let cnt++
done

