#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
//pipe_fd[2]是一个输出性参数！我们想通过这个参数读取到打开的两个fd 
int main()
{
    int pipe_fd[2] = {0};

    if(pipe(pipe_fd) < 0){
        perror("pipe");
        return 1;
    }
    //0(嘴)：读取端，1(笔)写入端
    printf("%d, %d\n", pipe_fd[0], pipe_fd[1]);
    //fork创建子进程
    pid_t id = fork();
    if(id < 0)//创建子进程失败
    {
        perror("fork");
        return 2;
    }
    //父进程读取，子进程写
    else if(id == 0) { //write
        //child
        close(pipe_fd[0]);

        //const char *msg = "hello parent, I am child";

        char c = 'x';
        int count = 0;
        //while(count){
        while(1){
            write(pipe_fd[1], &c, 1); //strlen(msg) + 1??
         //   sleep(1);
            count++;
            printf("write: %d\n", count);
        }

        close(pipe_fd[1]);
        exit(0);
    }
    else{              //read
        //parent
        close(pipe_fd[1]);

        char buffer[64];
        while(1){
            sleep(100);
            buffer[0] = 0;
            //如果read的返回值为0，意味着子进程关闭文件描述符了
            ssize_t size = read(pipe_fd[0], buffer, sizeof(buffer)-1);
            if(size > 0){
                buffer[size] = 0;
                printf("parent get messge from child# %s\n", buffer);
            }
            else if(size == 0){
                printf("pipe file close, child quit!\n");
                break;
            }
            else{
               //TODO
                break;
            }
        }

        //子进程返回信息
        int status = 0;
        if(waitpid(id, &status,0) > 0){
            printf("child quit, wait success!, sig: %d\n", status&0x7F);
        }
        close(pipe_fd[0]);
    }
    return 0;
}
